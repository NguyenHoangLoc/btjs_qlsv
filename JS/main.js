const STULIST = "STUDENTLIST";
var studenList = [];
var dataJson = localStorage.getItem(STULIST);
if (dataJson) {
  var dataRaw = JSON.parse(dataJson);
  var result = [];
  for (var index = 0; index < dataRaw.length; index++) {
    var currentData = dataRaw[index];
    var student = new Student(
      currentData.id,
      currentData.name,
      currentData.email,
      currentData.password,
      currentData.physPoint,
      currentData.mathPoint,
      currentData.chemPoint
    );
    result.push(student);
  }
  studenList = result;
  renderStudentList(studenList);
}
function saveLocalStorage() {
  var dssvJson = JSON.stringify(studenList);
  localStorage.setItem(STULIST, dssvJson);
}
//thêm SV
function addStudent() {
  var newStu = getData();

  studenList.push(newStu);
  saveLocalStorage();

  renderStudentList(studenList);
  resetForm();
}
//xóa SV
function deleteStudent(idStu) {
  var index = studenList.findIndex(function (student) {
    return student.id == idStu;
  });
  if (index == -1) {
    return;
  }

  studenList.splice(index, 1);

  renderStudentList(studenList);
  saveLocalStorage();
}
//sửa SV
function editStudent(idStu) {
  var index = studenList.findIndex(function (student) {
    return student.id == idStu;
  });
  if (index == -1) return;

  var student = studenList[index];

  showData(student);
  document.getElementById("txtMaSV").disabled = true;
}

function updateStudent() {
  var stuEdit = getData();
  var index = studenList.findIndex(function (student) {
    return student.id == stuEdit.id;
  });
  if (index == -1) return;

  studenList[index] = stuEdit;
  saveLocalStorage();
  renderStudentList(studenList);
  resetForm();
  document.getElementById("txtMaSV").disabled = false;
}
