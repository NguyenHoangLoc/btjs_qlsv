function Student(_id,_name,_email,_password,_mathPoint,_physPoint,_chemPoint){
    this.id=_id
    this.name=_name
    this.email=_email
    this.password=_password
    this.mathPoint=_mathPoint
    this.physPoint=_physPoint
    this.chemPoint=_chemPoint
    this.countAverage= function(){
        var average=(this.mathPoint+this.physPoint+this.chemPoint)/3
        return average.toFixed(1)
    }
        
    }
