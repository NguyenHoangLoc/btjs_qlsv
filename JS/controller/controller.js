function getData() {
  var idStudent = document.getElementById("txtMaSV").value;
  var nameStudent = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var password = document.getElementById("txtPass").value;
  var physPoint = document.getElementById("txtDiemLy").value * 1;
  var chemPoint = document.getElementById("txtDiemHoa").value * 1;
  var mathPoint = document.getElementById("txtDiemToan").value * 1;
  var student = new Student(
    idStudent,
    nameStudent,
    email,
    password,
    mathPoint,
    physPoint,
    chemPoint
  );
  return student;
}
function renderStudentList(list) {
  var contentHTML = "";
  for (var i = 0; i < list.length; i++) {
    var currentStu = list[i];
    var contentTr = `<tr> 
        <td>${currentStu.id}</td>
        <td>${currentStu.name}</td>
        <td>${currentStu.email}</td>
        <td>${currentStu.countAverage()}</td>
        <td>
        <button onclick="deleteStudent('${
          currentStu.id
        }')"  class="btn btn-danger">Xoá</button>
    
        <button
        
        onclick="editStudent('${currentStu.id}')"
        class="btn btn-primary">Sửa</button>
        </td>
        </tr>`;
    contentHTML = contentHTML + contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}
function showData(student) {
  document.getElementById("txtMaSV").value = student.id;
  document.getElementById("txtTenSV").value = student.name;
  document.getElementById("txtEmail").value = student.email;
  document.getElementById("txtPass").value = student.password;
  document.getElementById("txtDiemLy").value = student.physPoint;
  document.getElementById("txtDiemHoa").value = student.mathPoint;
  document.getElementById("txtDiemToan").value = student.chemPoint;
}

function resetForm() {
  document.getElementById("formQLSV").reset();
}
